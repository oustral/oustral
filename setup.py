from setuptools import setup, find_packages

exec(open(r"oustral/version.py")).read))

dependencies = ["selenium", "logging", "urllib3", "numpy"]

setup (
    name="oustral",
    version = __version__,
    description = "Overt User Simulator for overt content generation",
    long_description = "Provides an automated simulator to generate replaceable overt content for traffic-replacement-based censorship circumvention systems.",
    author = "Anna Lorimer",
    author_email = "annalorimer@uchicago.edu",
    license = "MIT",
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers", 
        "License :: OSI Approved :: MIT License",
        "Programming Lanugage :: Python :: 3.6"    
    ],
    keywords = ["oustral", "censorship", "censorship circumvention"],
    packages = find_packages(),
    install_requries = dependencies,
    setup_requires = dependencies,
)
