
import sys
import getopt

from oustral.modes import BaseMode
from oustral.drivers import FirefoxDriver

# Logging`
LOGFILE = "slifox_driver.log"

# Overt sites list
TEST_SITES = ["https://www.reddit.com","https://www.cbc.ca", "https://www.canadaland.com"]

OVERT_SITES = ["https://b.slitheen.net/r/cats/", "https://b.slitheen.net/r/SupermodelCats/", "https://b.slitheen.net/r/cutecats/", "https://a.slitheen.net", "https://c.slitheen.net"]


def print_usage():
    print("Usage: python slifox_driver.py -h | -u <user_mode>")

if __name__ == "__main__":
#    logging.basicConfig(filename=LOGFILE, format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s', level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S') 

    #Instantiate the driver you want to use
    driver = FirefoxDriver(geckodriver='/usr/local/bin/geckodriver')
    #instantiate the user mode you want to use
    user_mode = BaseMode()

    user_mode.add_driver(driver)
    user_mode.start()
    
