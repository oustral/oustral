#################################################
# This file implements the base of the OUStral
# driver. This driver should not instantiated 
# directly, instead instantiate the child class
# that corresponds to the browser you wish to 
# use.
#################################################


from selenium import webdriver
import logging
import numpy
from .parsers import YoutubeParser

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
import random
import numpy
import logging
from urllib.parse import urlparse

LOGFILE = "oustral.log"

# TODO: make these globals a config file
WEIBULL_SCALE = 30
WEIBULL_SHAPE = 0.75
ACTIONS = ["link", "new_addr", "history", "download", "nop"]
ACTION_PROBABILITIES = [0.451, 0.33, 0.098, 0.012, 0.109] 
OVERT_SITES = ["https://www.python.org", "https://www.youtube.com/"]

class Driver():

    def __init__(selfi, binary_file=False):
        pass

    def start(self):
        logging.info("OUStral has started ...")
            
    def stop(self):
        self.driver.close()
        logging.info("OUStral has stopped ...")

    #############################################
    # Below are the methods that perform 
    # browsing actions.
    #############################################
 
    def dwell(self):
            dwell_time = WEIBULL_SCALE * numpy.random.weibull(WEIBULL_SHAPE)
            print("Dwelling for " + str(dwell_time))
            time.sleep(dwell_time)

    # Navigate to a link on the same site
    def navigate_within_site(self):
        current_url = self.driver.current_url
        logging.info("Navigating within site ... ")
        links = self.driver.find_elements_by_xpath("//a[@href]")
        while True:
            try:
                link_url  = numpy.random.choice(links).get_attribute("href")
            except:			
            # Element may have become stale or some other error occurred
                logging.info("Something was wrong with that link ... trying another one")
                break

            domain = self.get_domain(link_url)

            if domain in current_url:
                logging.debug("Navigating to " + link_url)
                try:
                    self.driver.get(link_url)
                    break
                except Exception as ex:
                    logging.error("Exception while navigating within site: " + str(ex))

    def navigate_to_random_site(self):
            logging.info("Navigating to random site ... ")
             
            while True:
                new_site = random.choice(OVERT_SITES)
                current_domain = self.get_domain(self.driver.current_url)
                if new_site not in current_domain:
                    try:
                        logging.info("Navigating to " + new_site)            	
                        self.driver.get(new_site)
                        self.save_cookies()
                        break
                    except Exception as ex:
                        logging.error("Exception while trying to navigate to " + str(new_site) + ": " + str(ex))


    def navigate_to_site(self, url):
        logging.info("Navigating to " + url)
        
        try:
            self.driver.get(url)
            self.save_cookies()
        except Exception as ex:
            logging.error("Exception while trying to navigate to" + str(url) + ": " + str(ex))


    def download(self):
        self.dwell()
        
    # Misc helper functions
     
    def get_domain(self, url):
        parsed_url = urlparse(url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_url)
        return domain

class FirefoxDriver(Driver):

    def __init__(self, geckodriver=None, binary=None, yt_parser=False, tls_max_version=None, enable_http2=None):

        # Set up the driver

        fp = webdriver.FirefoxProfile()

        # Enable opening tabs
        fp.set_preference("browser.tabs.remote.autostart", False)
        fp.set_preference("browser.tabs.remote.autostart.1", False)
        fp.set_preference("browser.tabs.remote.autostart.2", False)

        # Set user agent
        fp.set_preference("general.useragent.override", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")

        if tls_max_version:
            fp.set_preference("security.tls.version.max", 3)
        
        if enable_http2:
            fp.set_preference("network.http.spdy.enabled.http2", False)


        if binary:
            firefox_binary = binary
        
        if geckodriver == None:
            print("You must specify the geckodriver path")
            sys.exit(1)
        print(geckodriver) 
        # Instantiate driver with the correct geckodriver version
        if binary and geckodriver:
            self.driver = webdriver.Firefox(firefox_profile=fp, executable_path = geckodriver, firefox_binary = binary) 
        else:
             self.driver = webdriver.Firefox(firefox_profile=fp, executable_path = geckodriver) 
 
            
        # Set page and script timeouts
        self.driver.set_page_load_timeout(30)
        self.driver.set_script_timeout(50000000)

        # Set up Youtube Parser
        if yt_parser:
            self.ytp = YoutubeParser(self.driver)
            
        logging.info("Slifox Driver started")


    def navigate_to_history(self):
        self.driver.execute_script("window.history.go(-1)")

    def open_tab(self):
        try:
            self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
            self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
            windows = self.driver.window_handles
            self.driver.switch_to_window(windows[-1])
        except Exception as e:
            logging.error(e)

    def close_tab(self):
        self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

    def save_cookies(self):
        try:
            cookies = self.driver.get_cookies()
            for cookie in cookies:
                self.driver.add_cookie(cookie)
        except Exception as e:
            logging.error("unable to save cookies")



