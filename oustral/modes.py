from .drivers import Driver
import numpy

ACTIONS = ["link", "new_addr", "history", "download", "nop"]
ACTION_PROBABILITIES = [0.451, 0.33, 0.098, 0.012, 0.109] 
OVERT_SITES = ["https://www.python.org", "https://www.youtube.com/"]


class BaseMode():

    def __init__(self):

        self.driver = None
        self.ACTIONS = []
        self.ACTION_PROBABILITIES = []
        self.OVERT_SITES = []

    def add_driver(self, driver):
        self.driver=driver    

    def start(self):
        while True:
            self.action()
            self.driver.dwell()

    def parse_action_config(self):
        try:
            config_file = open("actions.config")
        except Exception as e:
            raise Exception(e)

        probabilities = []
        actions = []
        action_probs_dict = {}

        for line in config_file.readlines():
            action_prob = line.split(",")
            
            try:
                action = action_prob[0]
                prob = float(action_prob[1])
            except Exception as e:
                raise Exception(e)

            action_probs_dict[action] = prob
            actions.append(action)
            probabilities.append(prob)

        if sum(probabilities) != 1:
            raise ValueError("Error parsing action.config: Action probabilities do not sum to 1")
        
        for action in action:
           if action not in ACTIONS:
                raise Exception("Invalid action: " + action + ". See documentation.") 

        self.ACTIONS = actions
        self.ACTION_PROBABILITIES = probabilities
    
    
    def parse_overt_sites(self):
        try:
            f = open("overt_sites.config")
        except Exception as e:
            raise Exception(e)
        
        for site in f.readlines():
            self.OVERT_SITES.append(site)

        if len(self.OVERT_SITES) < 1:
            raise ValueError("At least one overt site must be specified.")

    def action(self):
        action = numpy.random.choice(ACTIONS, p=ACTION_PROBABILITIES)
        print("Next action: " + action)
        
        if action == "link": 
            self.driver.navigate_within_site()
        elif action == "new_addr": 
            self.driver.navigate_to_random_site()
        elif action == "history": 
            self.driver.navigate_to_history()
        elif action ==  "download": 
            self.driver.downlaod()
        elif action == "new_tab":
            self.driver.new_tab()
        elif action == "switch_tab": 
            self.driver.switch_tab()
        elif action == "close_tab": 
            self.driver.close_tab()
        elif action == "nop":
            self.driver.dwell()
        else:
            raise ValueError("Invalid action: " + action)

class BackgroundVideoUser(BaseMode):
    '''
    This user model uses two tabs to simulate a user.
    The first tab is a never ending YouTube live stream,
    the other follow the base UserModel.
    '''
    def start(self):
        #logging.info("Starting background video ...")
        self.driver.navigate_to_site("https://www.youtube.com/watch?v=QtTh6vgMzAY") # This is the neverending bob's burgers live stream
        self.driver.dwell()
        self.driver.open_tab()
        while True:
            self.driver.dwell()
            self.action()

class VideoUser(BaseMode):
    '''
    This user model uses one tab to watch YouTube videos.
    Notably, this user model can detect when a video finnishes
    and will click on a recommended video.
    '''

    def start(self):
        self.navigate_to_site("https://www.youtube.com")
        
        # Navigate to first video
        self.driver.navigate_to_site(self.driver.ytp.home_page_select())
        self.slifox_driver.ytp.disable_autoplay()

        while True:
            self.driver.ytp.click_play()
            self.driver.ytp.detect_video_ended()
            self.driver.dwell()
            self.driver.navigate_to_site(self.driver.ytp.secondary_recommendations_select())


